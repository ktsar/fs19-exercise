package me.superup.fs19ktsartsaros.app.service;

import me.superup.fs19ktsartsaros.app.model.Call;
import me.superup.fs19ktsartsaros.app.model.Caller;
import me.superup.fs19ktsartsaros.app.model.CallerContact;
import org.springframework.data.domain.Page;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

public interface ICallService
{

   void findAll(DeferredResult<List<Call>> result);

   void findPaginated(int page, int size, DeferredResult<Page<Call>> result);

   void save(Call call, DeferredResult<Call> result);

   void update(Call call, DeferredResult<Call> result);

   void delete(Long id, DeferredResult<Call> result);

   void callContacts(Caller caller, DeferredResult<List<CallerContact>> result);

}
