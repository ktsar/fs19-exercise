package me.superup.fs19ktsartsaros.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "callers")
public class Caller extends AbstractModel
{

   @JsonProperty("first_name")
   @Column(name = "first_name", nullable = false)
   private String firstName;

   @JsonProperty("last_name")
   @Column(name = "last_name", nullable = false)
   private String lastName;

   @JsonProperty("email")
   @Column(name = "email", nullable = false)
   private String email;

   @JsonProperty("gender")
   @Column(name = "gender", nullable = false)
   private Gender gender;

   @JsonProperty("image")
   @Column(name = "image")
   private String image;

   @OneToMany(mappedBy = "caller")
   @JsonIgnore
   private List<Call> outgoingCalls;

   @OneToMany(mappedBy = "calledParty")
   @JsonIgnore
   private List<Call> incomingCalls;

   /**
    * Creates a {@link Caller} reference entity for JSON deserialization
    *
    * @param callerId The {@link Caller} id
    * @return A reference {@link Caller} instance that only contains an ID
    */
   public static Caller fromId(final Long callerId)
   {
      Caller caller = new Caller();
      caller.id = callerId;
      return caller;
   }

   /**
    * Creates a {@link Caller} instance for repository example queries.
    *
    * @param query The query specified by the lookup operation
    * @return A {@link Caller} instance with firstName, lastName and email properties
    * set to the query value so that it can be used for example queries
    */
   public static Caller forLookup(final String query)
   {
      Caller caller = new Caller();
      caller.firstName = query;
      caller.lastName = query;
      caller.email = query;
      return caller;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public Gender getGender()
   {
      return gender;
   }

   public void setGender(Gender gender)
   {
      this.gender = gender;
   }

   public String getImage()
   {
      return image;
   }

   public void setImage(String image)
   {
      this.image = image;
   }

   public List<Call> getOutgoingCalls()
   {
      return outgoingCalls;
   }

   public List<Call> getIncomingCalls()
   {
      return incomingCalls;
   }

   public void setIncomingCalls(List<Call> incomingCalls)
   {
      this.incomingCalls = incomingCalls;
   }
}
