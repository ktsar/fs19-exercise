package me.superup.fs19ktsartsaros.app.service.impl;

import me.superup.fs19ktsartsaros.app.model.Call;
import me.superup.fs19ktsartsaros.app.model.Caller;
import me.superup.fs19ktsartsaros.app.model.CallerContact;
import me.superup.fs19ktsartsaros.app.repository.CallRepository;
import me.superup.fs19ktsartsaros.app.service.ICallService;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.async.DeferredResult;

import javax.persistence.EntityNotFoundException;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class CallServiceRepoImpl implements ICallService
{

   private final CallRepository callRepo;

   public CallServiceRepoImpl(CallRepository callerRepo)
   {
      this.callRepo = callerRepo;
   }

   @Override public void findAll(DeferredResult<List<Call>> result)
   {
      result.setResult(callRepo.findAll());
   }

   @Override public void findPaginated(int page, int size, DeferredResult<Page<Call>> result)
   {
      result.setResult(callRepo.findAll(PageRequest.of(page, size)));
   }

   @Override public void save(Call call, DeferredResult<Call> result)
   {
      result.setResult(callRepo.save(call));
   }

   @Override public void update(Call call, DeferredResult<Call> result)
   {
      Optional<Call> existingOpt = callRepo.findById(call.getId());
      if (!existingOpt.isPresent()) {
         result.setErrorResult(new EntityNotFoundException(String.format("Could not find call record (id=%d)",
               call.getId())));
      }
      final Call existing = existingOpt.get();
      existing.update(call);
      result.setResult(existing);
   }

   @Override public void delete(Long id, DeferredResult<Call> result)
   {
      Optional<Call> existingOpt = callRepo.findById(id);
      if (!existingOpt.isPresent()) {
         result.setErrorResult(new EntityNotFoundException(String.format("Could not find call record (id=%d)", id)));
      }
      final Call existing = existingOpt.get();
      callRepo.delete(existing);
      result.setResult(existing);
   }

   /**
    * Returns a {@link DateTimeFormatter} for handling date-time formats specific
    * to the request context's locale
    *
    * @return
    */
   private DateTimeFormatter getDateTimeFormatter()
   {
      return DateTimeFormatter.ofPattern("MMM yyyy", LocaleContextHolder.getLocale());
   }

   private Map<String, List<Call>> getOutgoingCallsByMonth(Caller caller, Caller calledParty)
   {
      final DateTimeFormatter monthlyDtf = getDateTimeFormatter();
      final Map<String, List<Call>> results = caller.getOutgoingCalls().stream()
            .filter(call -> call.getCalledParty().equals(calledParty))
            .collect(Collectors.groupingBy(o -> o.getTimestamp().format(monthlyDtf),
                  LinkedHashMap::new, Collectors.toList()));
      return results;
   }

   @Override public void callContacts(Caller caller, DeferredResult<List<CallerContact>> result)
   {
      final List<CallerContact> results = new ArrayList<>();

      for (Caller recentContact : callRepo.callerContacts(caller)) {
         final Map<String, List<Call>> monthlyCalls = getOutgoingCallsByMonth(caller, recentContact);
         final Long totalCallDuration = monthlyCalls.values().parallelStream()
               .flatMap(calls -> calls.stream())
               .map(call -> call.getDurationInSeconds())
               .collect(Collectors.summingLong(Integer::intValue));
         results.add(new CallerContact(recentContact, monthlyCalls, totalCallDuration));
      }
      result.setResult(results);
   }
}
