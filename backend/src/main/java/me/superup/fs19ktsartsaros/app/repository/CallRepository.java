package me.superup.fs19ktsartsaros.app.repository;

import me.superup.fs19ktsartsaros.app.model.Call;
import me.superup.fs19ktsartsaros.app.model.Caller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CallRepository extends JpaRepository<Call, Long>
{
   List<Call> findByTimestampBetween(@Param("from") LocalDateTime from, @Param("until") LocalDateTime until);

   List<Call> findAllByCallerAndCalledPartyOrderByTimestampDesc(@Param("caller") Caller caller,
         @Param("calledParty") Caller calledParty);

   @Query("select distinct call.calledParty as contact from Call call where call.caller = ?1 order by call.calledParty.lastName desc")
   List<Caller> callerContacts(@Param("caller") Caller caller);
}
