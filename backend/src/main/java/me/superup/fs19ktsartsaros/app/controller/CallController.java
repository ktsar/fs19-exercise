package me.superup.fs19ktsartsaros.app.controller;

import me.superup.fs19ktsartsaros.app.model.Call;
import me.superup.fs19ktsartsaros.app.service.ICallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

@RestController
@RequestMapping("/calls")
@CrossOrigin
public class CallController {

   private ICallService callService;

   @Autowired
   public CallController(ICallService callService) {
      this.callService = callService;
   }

   @GetMapping
   public DeferredResult<List<Call>> findAll() {
      DeferredResult<List<Call>> result = new DeferredResult<>();
      callService.findAll(result);
      return result;
   }

   @GetMapping(params = { "page", "size" })
   public DeferredResult<Page<Call>> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
      DeferredResult<Page<Call>> result = new DeferredResult<>();
      callService.findPaginated(page, size, result);
      return result;
   }

   @PatchMapping
   public DeferredResult<Call> update(@RequestBody Call call) {
      DeferredResult<Call> result = new DeferredResult<>();
      callService.update(call, result);
      return result;
   }

   @PutMapping
   public DeferredResult<Call> save(@RequestBody Call call) {
      DeferredResult<Call> result = new DeferredResult<>();
      callService.save(call, result);
      return result;
   }

   @DeleteMapping(value = "/{id}")
   public DeferredResult<Call> delete(@PathVariable("id") Long id) {
      DeferredResult<Call> result = new DeferredResult<>();
      callService.delete(id, result);
      return result;
   }

}
