package me.superup.fs19ktsartsaros.app.controller;

import me.superup.fs19ktsartsaros.app.model.Caller;
import me.superup.fs19ktsartsaros.app.model.CallerContact;
import me.superup.fs19ktsartsaros.app.service.ICallService;
import me.superup.fs19ktsartsaros.app.service.ICallerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

@RestController
@RequestMapping("/callers")
public class CallerController
{

   private ICallerService callerService;
   private ICallService callService;

   @Autowired
   public CallerController(ICallerService callerService, ICallService callService)
   {
      this.callerService = callerService;
      this.callService = callService;
   }

   @GetMapping
   public DeferredResult<List<Caller>> findAll()
   {
      DeferredResult<List<Caller>> result = new DeferredResult<>();
      callerService.findAll(result);
      return result;
   }

   @GetMapping(params = { "page", "size" })
   public DeferredResult<Page<Caller>> findPaginated(@RequestParam("page") int page,
         @RequestParam("size") int size)
   {
      DeferredResult<Page<Caller>> result = new DeferredResult<>();
      callerService.findPaginated(page, size, result);
      return result;
   }

   @GetMapping(value = "/lookup", params = { "query" })
   public DeferredResult<List<Caller>> lookup(@RequestParam("query") String query)
   {
      DeferredResult<List<Caller>> result = new DeferredResult<>();
      callerService.lookup(query, result);
      return result;
   }

   @GetMapping(value = "/contacted", params = { "caller_id" })
   public DeferredResult<List<CallerContact>> contacted(@RequestParam("caller_id") Caller caller)
   {
      DeferredResult<List<CallerContact>> result = new DeferredResult<>();
      callService.callContacts(caller, result);
      return result;
   }

   @PostMapping
   public DeferredResult<Caller> save(@RequestBody Caller caller)
   {
      DeferredResult<Caller> result = new DeferredResult<>();
      callerService.save(caller, result);
      return result;
   }

}