package me.superup.fs19ktsartsaros.app.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "calls")
public class Call extends AbstractModel
{

   @ManyToOne(fetch = FetchType.LAZY, optional = false)
   @JoinColumn(name = "caller_id", nullable = false)
   @OnDelete(action = OnDeleteAction.CASCADE)
   @JsonProperty("caller_id")
   @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
   @JsonIdentityReference(alwaysAsId = true)
   private Caller caller;

   @ManyToOne(fetch = FetchType.LAZY, optional = false)
   @JoinColumn(name = "called_party_id", nullable = false)
   @OnDelete(action = OnDeleteAction.CASCADE)
   @JsonProperty("called_party_id")
   @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
   @JsonIdentityReference(alwaysAsId = true)
   private Caller calledParty;

   @JsonProperty("timestamp")
   @Column(name = "timestamp", nullable = false)
   @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
   @JsonDeserialize(using = LocalDateTimeDeserializer.class)
   @JsonSerialize(using = LocalDateTimeSerializer.class)
   private LocalDateTime timestamp;

   @JsonProperty("duration_in_seconds")
   @Column(name = "duration_in_seconds", nullable = false)
   private Integer durationInSeconds;

   public Caller getCaller()
   {
      return caller;
   }

   @JsonProperty("caller_id")
   public void setCaller(Long callerId)
   {
      this.caller = Caller.fromId(callerId);
   }

   public Caller getCalledParty()
   {
      return calledParty;
   }

   @JsonProperty("called_party_id")
   public void setCalledParty(Long calledPartyId)
   {
      this.calledParty = Caller.fromId(calledPartyId);
   }

   public LocalDateTime getTimestamp()
   {
      return timestamp;
   }

   public void setTimestamp(LocalDateTime timestamp)
   {
      this.timestamp = timestamp;
   }

   public Integer getDurationInSeconds()
   {
      return durationInSeconds;
   }

   public void setDurationInSeconds(Integer durationInSeconds)
   {
      this.durationInSeconds = durationInSeconds;
   }

   public void update(final Call other)
   {
      this.caller = other.caller != null ? other.caller : this.caller;
      this.calledParty = other.calledParty != null ? other.calledParty : this.calledParty;
      this.timestamp = other.timestamp != null ? other.timestamp : this.timestamp;
      this.durationInSeconds = other.durationInSeconds != null ? other.durationInSeconds : this.durationInSeconds;
   }
}
