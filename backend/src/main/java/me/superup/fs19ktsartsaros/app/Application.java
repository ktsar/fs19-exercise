package me.superup.fs19ktsartsaros.app;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.superup.fs19ktsartsaros.app.model.Call;
import me.superup.fs19ktsartsaros.app.model.Caller;
import me.superup.fs19ktsartsaros.app.repository.CallRepository;
import me.superup.fs19ktsartsaros.app.repository.CallerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableAsync;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SpringBootApplication
@EnableAsync
public class Application
{

   public static void main(String[] args)
   {
      SpringApplication.run(Application.class, args);
   }

   @Bean
   @Profile("init")
   CommandLineRunner runner(CallerRepository callerRepo, CallRepository callRepo, ObjectMapper mapper)
   {
      return args -> {
         // read caller data and write to db
         TypeReference<List<Caller>> callerTypeRef = new TypeReference<List<Caller>>()
         {
         };
         InputStream is1 = TypeReference.class.getResourceAsStream("/data/callers_mock_data.json");
         try {
            List<Caller> callers = mapper.readValue(is1, callerTypeRef);
            callerRepo.saveAll(callers);
            System.out.println(String.format("Loaded %d callers", callers.size()));
         }
         catch (IOException e) {
            System.out.println("Unable to load callers: " + e.getMessage());
         }

         // read call records and write to db
         TypeReference<List<Call>> callTypeRef = new TypeReference<List<Call>>()
         {
         };
         InputStream is2 = TypeReference.class.getResourceAsStream("/data/calls_mock_data.json");
         try {
            List<Call> calls = mapper.readValue(is2, callTypeRef);
            callRepo.saveAll(calls);
            System.out.println(String.format("Loaded %d call records", calls.size()));
         }
         catch (IOException e) {
            System.out.println("Unable to load calls: " + e.getMessage());
         }

         System.out.println("Initialization complete. Please start application without the \"init\" profile.");
         System.exit(0);
      };
   }
}
