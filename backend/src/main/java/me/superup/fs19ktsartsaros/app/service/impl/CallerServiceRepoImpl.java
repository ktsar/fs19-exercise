package me.superup.fs19ktsartsaros.app.service.impl;

import me.superup.fs19ktsartsaros.app.model.Caller;
import me.superup.fs19ktsartsaros.app.repository.CallerRepository;
import me.superup.fs19ktsartsaros.app.service.ICallerService;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

@Service
@Transactional
public class CallerServiceRepoImpl implements ICallerService
{

   private final CallerRepository callerRepo;

   public CallerServiceRepoImpl(CallerRepository callerRepo)
   {
      this.callerRepo = callerRepo;
   }

   @Override public void findAll(DeferredResult<List<Caller>> result)
   {
      result.setResult(callerRepo.findAll());
   }

   @Override public void findPaginated(int page, int size, DeferredResult<Page<Caller>> result)
   {
      result.setResult(callerRepo.findAll(PageRequest.of(page, size)));
   }

   @Override public void lookup(String query, DeferredResult<List<Caller>> result)
   {
      final ExampleMatcher lookupMatcher = ExampleMatcher.matchingAny()
            .withMatcher("firstName", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
            .withMatcher("lastName", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
            .withMatcher("email", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
            .withIgnorePaths("id");

      final Example<Caller> example = Example.of(Caller.forLookup(query), lookupMatcher);

      result.setResult(callerRepo.findAll(example));
   }

   @Override public void save(Caller caller, DeferredResult<Caller> result)
   {
      result.setResult(callerRepo.save(caller));
   }
}
