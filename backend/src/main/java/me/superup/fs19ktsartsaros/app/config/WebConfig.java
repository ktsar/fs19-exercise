package me.superup.fs19ktsartsaros.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

   @Override
   @Profile("dev")
   public void addCorsMappings(CorsRegistry registry) {
      registry.addMapping("/**");
   }

}
