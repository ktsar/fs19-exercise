package me.superup.fs19ktsartsaros.app.repository;

import me.superup.fs19ktsartsaros.app.model.Caller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CallerRepository extends JpaRepository<Caller, Long>
{
}
