package me.superup.fs19ktsartsaros.app.service;

import me.superup.fs19ktsartsaros.app.model.Caller;
import org.springframework.data.domain.Page;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

public interface ICallerService
{

   void findAll(DeferredResult<List<Caller>> result);

   void findPaginated(int page, int size, DeferredResult<Page<Caller>> result);

   void lookup(String query, DeferredResult<List<Caller>> result);

   void save(Caller caller, DeferredResult<Caller> result);

}
