package me.superup.fs19ktsartsaros.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

public class CallerContact
{

   private final Caller contact;
   private final Map<String, List<Call>> calls;
   @JsonProperty("total_call_duration")
   private final Long totalCallDuration;

   public CallerContact(Caller contact, Map<String, List<Call>> calls, Long totalCallDuration)
   {
      this.contact = contact;
      this.calls = calls;
      this.totalCallDuration = totalCallDuration;
   }

   public Caller getContact()
   {
      return contact;
   }

   public Map<String, List<Call>> getCalls()
   {
      return calls;
   }

   public Long getTotalCallDuration()
   {
      return totalCallDuration;
   }
}
