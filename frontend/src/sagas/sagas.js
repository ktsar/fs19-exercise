import { call, put, putResolve, takeLatest } from "redux-saga/effects";
import {
   ADD_CALL,
   ADD_CALL_FAIL,
   ADD_CALL_SUCCESS,
   DELETE_CALL,
   DELETE_CALL_FAIL,
   DELETE_CALL_SUCCESS,
   FETCH_CALLERS,
   FETCH_CALLERS_FAIL,
   FETCH_CALLERS_SUCCESS,
   FETCH_CALL_INFO,
   FETCH_CALL_INFO_FAIL,
   FETCH_CALL_INFO_SUCCESS,
   UPDATE_CALL,
   UPDATE_CALL_FAIL,
   UPDATE_CALL_SUCCESS
} from "../actions/types";
import {
   addCall,
   deleteCall,
   fetchCallers,
   fetchCallInfo,
   updateCall
} from "../api";

function* fetchCallersFn() {
   try {
      const data = yield call(fetchCallers);
      yield putResolve({ type: FETCH_CALLERS_SUCCESS, data });
   } catch (e) {
      yield putResolve({ type: FETCH_CALLERS_FAIL, message: e.message });
   }
}

function* fetchCallInfoFn(action) {
   try {
      const data = yield call(fetchCallInfo, action.callerId);
      yield putResolve({
         type: FETCH_CALL_INFO_SUCCESS,
         data: { caller: action.callerId, contacts: data }
      });
   } catch (e) {
      yield putResolve({ type: FETCH_CALL_INFO_FAIL, message: e.message });
   }
}

function* addCallFn(action) {
   try {
      const data = yield call(addCall, action.call);
      yield putResolve({ type: ADD_CALL_SUCCESS, data });
   } catch (e) {
      yield putResolve({ type: ADD_CALL_FAIL, message: e.message });
   }
}

function* addCallSuccessFn(action) {
   yield put({ type: FETCH_CALL_INFO, callerId: action.data.caller_id });
}

function* updateCallFn(action) {
   try {
      const data = yield call(updateCall, action.call);
      yield putResolve({ type: UPDATE_CALL_SUCCESS, data });
   } catch (e) {
      yield putResolve({ type: UPDATE_CALL_FAIL, message: e.message });
   }
}

function* updateCallSuccessFn(action) {
   yield put({ type: FETCH_CALL_INFO, callerId: action.data.caller_id });
}

function* deleteCallFn(action) {
   try {
      const data = yield call(deleteCall, action.call);
      yield putResolve({ type: DELETE_CALL_SUCCESS, data });
   } catch (e) {
      yield putResolve({ type: DELETE_CALL_FAIL, message: e.message });
   }
}

function* deleteCallSuccessFn(action) {
   yield put({ type: FETCH_CALL_INFO, callerId: action.data.caller_id });
}

function* rootSaga() {
   yield takeLatest(FETCH_CALLERS, fetchCallersFn);
   yield takeLatest(FETCH_CALL_INFO, fetchCallInfoFn);
   yield takeLatest(ADD_CALL, addCallFn);
   yield takeLatest(ADD_CALL_SUCCESS, addCallSuccessFn);
   yield takeLatest(UPDATE_CALL, updateCallFn);
   yield takeLatest(UPDATE_CALL_SUCCESS, updateCallSuccessFn);
   yield takeLatest(DELETE_CALL, deleteCallFn);
   yield takeLatest(DELETE_CALL_SUCCESS, deleteCallSuccessFn);
}

export default rootSaga;
