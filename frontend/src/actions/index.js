import { ADD_CALL, DELETE_CALL, FETCH_CALLERS, FETCH_CALL_INFO, SELECT_CALLER, SELECT_CONTACT, UPDATE_CALL } from "./types";

export const fetchCallers = () => {
   return { type: FETCH_CALLERS };
};

export const fetchCallInfo = callerId => {
   return { type: FETCH_CALL_INFO, callerId };
};

export const selectCaller = callerId => {
   return { type: SELECT_CALLER, callerId };
};

export const selectContact = contactId => {
   return { type: SELECT_CONTACT, contactId };
};

export const addCall = call => {
   return { type: ADD_CALL, call };
};

export const updateCall = call => {
   return { type: UPDATE_CALL, call };
};

export const deleteCall = call => {
   return { type: DELETE_CALL, call };
};
