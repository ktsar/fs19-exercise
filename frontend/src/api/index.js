const FETCH_CALLERS_ENDPOINT = "http://localhost:8080/callers";
const FETCH_CALL_INFO_ENDPOINT = "http://localhost:8080/callers/contacted";
const CALLS_ENDPOINT = "http://localhost:8080/calls";

export const fetchCallers = () =>
   fetch(FETCH_CALLERS_ENDPOINT)
      .then(response => response.json())
      .catch(error => console.error(error));

export const fetchCallInfo = callerId =>
   fetch(FETCH_CALL_INFO_ENDPOINT + `?caller_id=${callerId}`)
      .then(response => response.json())
      .catch(error => console.error(error));

export const addCall = call =>
   fetch(CALLS_ENDPOINT, {
      method: "PUT",
      headers: {
         Accept: "application/json",
         "Content-Type": "application/json"
      },
      body: JSON.stringify(call)
   })
      .then(response => response.json())
      .catch(error => console.error(error));

export const updateCall = call =>
   fetch(CALLS_ENDPOINT, {
      method: "PATCH",
      headers: {
         Accept: "application/json",
         "Content-Type": "application/json"
      },
      body: JSON.stringify(call)
   })
      .then(response => response.json())
      .catch(error => console.error(error));

export const deleteCall = call =>
   fetch(CALLS_ENDPOINT + `/${call.id}`, {
      method: "DELETE",
      headers: {
         Accept: "application/json",
         "Content-Type": "application/json"
      }
   })
      .then(response => response.json())
      .catch(error => console.error(error));
