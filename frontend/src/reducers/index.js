import { cloneDeep } from "lodash";
import { FETCH_CALLERS_SUCCESS, FETCH_CALL_INFO_SUCCESS, SELECT_CALLER, SELECT_CONTACT } from "../actions/types";

const initialState = {
   callers: [],
   callInfo: {},
   selectedCaller: -1,
   selectedContact: -1
};

function rootReducer(state = initialState, action) {
   let newState;
   switch (action.type) {
      case FETCH_CALLERS_SUCCESS:
         newState = cloneDeep(state);
         newState.callers = action.data;

         return Object.assign({}, state, newState);
      case FETCH_CALL_INFO_SUCCESS:
         newState = cloneDeep(state);
         newState.callInfo[action.data.caller] = action.data.contacts;

         return Object.assign({}, state, newState);
      case SELECT_CALLER:
         return Object.assign({}, state, {
            selectedCaller: action.callerId
         });
      case SELECT_CONTACT:
         return Object.assign({}, state, {
            selectedContact: action.contactId
         });
      default:
         return state;
   }
}

export default rootReducer;
