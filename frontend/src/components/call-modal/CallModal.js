import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import React from "react";
import "./CallModal.css";

const initialState = {
   open: false,
   call: {
      id: -1,
      caller_id: -1,
      called_party_id: -1,
      timestamp: "",
      duration_in_seconds: -1
   }
};

class CallModal extends React.Component {
   constructor(props) {
      super(props);
      this.state = initialState;

      this.submit = this.submit.bind(this);
      this.isUpdateModal = this.isUpdateModal.bind(this);
      this.handleChange = this.handleChange.bind(this);
   }

   componentDidUpdate(prevProps) {
      if (this.props.open !== prevProps.open) {
         this.setState({
            open: this.props.open,
            call: this.props.call
         });
      }
   }

   submit() {
      this.props.onSubmit(this.state.call);
   }

   isUpdateModal() {
      return this.state.call.id >= 0;
   }

   handleChange(event) {
      const call = {
         ...this.state.call,
         [event.target.id]: event.target.value
      };
      this.setState({
         call
      });
   }

   render() {
      const { onClose } = this.props;
      return (
         <Dialog
            open={this.state.open}
            onClose={onClose}
            aria-labelledby="form-dialog-title"
         >
            <DialogTitle id="form-dialog-title">
               {this.isUpdateModal() ? "Update" : "Add"} call
            </DialogTitle>
            <DialogContent>
               <TextField
                  autoFocus
                  margin="dense"
                  id="timestamp"
                  label="Timestamp"
                  type="text"
                  value={this.state.call.timestamp}
                  onChange={this.handleChange}
                  fullWidth
               />
               <TextField
                  margin="dense"
                  id="duration_in_seconds"
                  label="Duration in seconds"
                  type="text"
                  value={this.state.call.duration_in_seconds}
                  onChange={this.handleChange}
                  fullWidth
               />
            </DialogContent>
            <DialogActions>
               <Button onClick={onClose}>Cancel</Button>
               <Button onClick={this.submit} color="primary">
                  {this.isUpdateModal() ? "Update" : "Add"}
               </Button>
            </DialogActions>
         </Dialog>
      );
   }
}

export default CallModal;
