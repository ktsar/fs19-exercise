import { Paper } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import React from "react";
import { connect } from "react-redux";
import { fetchCallInfo } from "../../actions/index";
import "./ContactList.css";

const fullName = caller => {
   return caller.first_name + " " + caller.last_name;
};

const dispatcher = dispatch => {
   return {
      fetchCallInfo: callerId => dispatch(fetchCallInfo(callerId))
   };
};

const select = store => {
   return { callInfo: store.callInfo, callerId: store.selectedCaller };
};

const initialState = {
   callerId: -1,
   contacts: []
};

class ConnectedContactList extends React.Component {
   constructor(props) {
      super(props);
      this.state = initialState;
   }

   componentDidUpdate(prevProps) {
      // If detecting selected caller change fetch the call info
      if (this.props.callerId !== prevProps.callerId) {
         this.setState({
            callerId: this.props.callerId
         });
         this.props.fetchCallInfo(this.props.callerId);
         return;
      }

      // If caller has been selected and call info is ready update the compoenent
      if (
         this.props.callInfo !== prevProps.callInfo &&
         this.props.callerId >= 0 &&
         this.props.callInfo[this.props.callerId] != null
      ) {
         this.setState({
            contacts: this.props.callInfo[this.props.callerId].map(
               item => item.contact
            )
         });
      }
   }

   render() {
      const { selectContact } = this.props;
      return this.state.callerId >= 0 && this.state.contacts != null ? (
         <Paper className="contact-list-wrapper">
            <List
               className="contact-list"
               component="nav"
               aria-label="main contacted"
            >
               {this.state.contacts.map(contact => (
                  <ListItem
                     key={contact.id}
                     onClick={() => selectContact(contact.id)}
                     button
                  >
                     <ListItemText primary={fullName(contact)} />
                  </ListItem>
               ))}
            </List>
         </Paper>
      ) : (
         <div>Please select a caller</div>
      );
   }
}

const ContactList = connect(select, dispatcher)(ConnectedContactList);

export default ContactList;
