import { Grid } from "@material-ui/core";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import * as moment from "moment";
import React from "react";
import { connect } from "react-redux";
import { addCall, deleteCall, updateCall } from "../../actions";
import CallModal from "../call-modal/CallModal";
import "./CallInfo.css";

const indexOfContactInCallInfo = (contactId, callerCallInfo) => {
   let i = 0;
   for (let item of callerCallInfo) {
      if (item.contact.id === contactId) {
         return i;
      }
      i++;
   }
};

const select = store => {
   return {
      callInfo: store.callInfo
   };
};

const dispatcher = dispatch => {
   return {
      addCall: call => dispatch(addCall(call)),
      updateCall: call => dispatch(updateCall(call)),
      deleteCall: call => dispatch(deleteCall(call))
   };
};

const initialState = {
   initialized: false,
   calls: {},
   totalCallDuration: -1,
   expanded: "none",
   activeCall: {},
   modalOpen: false
};

const renderCallItem = (month, call, openModal, deleteCall) => {
   return (
      <div className="call-item">
         <div>{call.timestamp}</div>
         <div className="call-item-duration">
            <span>Duration: </span>
            {call.duration_in_seconds} sec
         </div>
         <div className="call-item-actions">
            <IconButton
               onClick={() => openModal(month, call.id)}
               aria-label="edit"
            >
               <EditIcon />
            </IconButton>
            <IconButton onClick={() => deleteCall(call)} aria-label="delete">
               <DeleteIcon />
            </IconButton>
         </div>
      </div>
   );
};

class ConnectedCallInfo extends React.Component {
   constructor(props) {
      super(props);
      this.state = initialState;

      this.expand = this.expand.bind(this);
      this.openModal = this.openModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
      this.getCall = this.getCall.bind(this);
      this.deleteCall = this.deleteCall.bind(this);
      this.submitModal = this.submitModal.bind(this);
   }

   expand(key) {
      if (this.state.expanded === key) {
         this.setState({
            expanded: "none"
         });
      } else {
         this.setState({
            expanded: key
         });
      }
   }

   openModal(month, callId) {
      if (month === null && callId === month) {
         this.setState({
            modalOpen: true,
            activeCall: {
               id: -1,
               caller_id: this.props.callerId,
               called_party_id: this.props.contactId,
               timestamp: moment()
                  .format("YYYY-MM-DD HH:mm:ss")
                  .toString(),
               duration_in_seconds: 0
            }
         });
      } else {
         this.setState({
            modalOpen: true,
            activeCall: this.getCall(month, callId)
         });
      }
   }

   closeModal() {
      this.setState({
         modalOpen: false,
         activeCall: {
            id: -1,
            caller_id: -1,
            called_party_id: -1,
            timestamp: "",
            duration_in_seconds: -1
         }
      });
   }

   submitModal(call) {
      if (call.id < 0) {
         this.props.addCall(call);
      } else {
         this.props.updateCall(call);
      }
      this.closeModal();
   }

   getCall(month, callId) {
      const call = this.state.calls[month].filter(
         call => call.id === callId
      )[0];
      return call;
   }

   deleteCall(call) {
      this.props.deleteCall(call);
   }

   componentDidUpdate(prevProps) {
      // If a caller and a contact has been selected update the component
      if (
         (this.props.contactId !== prevProps.contactId &&
            this.props.callerId >= 0 &&
            this.props.contactId >= 0) ||
         this.state.shouldUpdate
      ) {
         const callerCallInfo = this.props.callInfo[this.props.callerId];
         const contactIdx = indexOfContactInCallInfo(
            this.props.contactId,
            callerCallInfo
         );
         if (contactIdx >= 0) {
            this.setState({
               initialized: true,
               calls: callerCallInfo[contactIdx].calls,
               totalCallDuration:
                  callerCallInfo[contactIdx].total_call_duration,
               expanded: initialState.expanded
            });
         }
      }
   }

   render() {
      return this.state.initialized ? (
         <div>
            <CallModal
               open={this.state.modalOpen}
               call={this.state.activeCall}
               onClose={this.closeModal}
               onSubmit={this.submitModal}
            />
            <Grid
               container
               direction="column"
               justify="flex-start"
               alignItems="center"
            >
               <Grid item className="row">
                  <div className="caller-history-header">
                     <h4>Call history</h4>
                     <div className="caller-history-header-actions">
                        <IconButton
                           onClick={() => this.openModal(null, null)}
                           aria-label="add"
                        >
                           <AddIcon style={{ color: "#fff" }} />
                        </IconButton>
                     </div>
                  </div>
                  {Object.keys(this.state.calls).map(month => (
                     <ExpansionPanel
                        key={month}
                        className="panel"
                        expanded={
                           this.state.expanded ===
                           "panel" +
                              Object.keys(this.state.calls).indexOf(month)
                        }
                        onClick={() =>
                           this.expand(
                              "panel" +
                                 Object.keys(this.state.calls).indexOf(month)
                           )
                        }
                     >
                        <ExpansionPanelSummary
                           expandIcon={<ExpandMoreIcon />}
                           aria-controls={
                              "panel" +
                              Object.keys(this.state.calls).indexOf(month) +
                              "bh-content"
                           }
                           id={
                              "panel" +
                              Object.keys(this.state.calls).indexOf(month) +
                              "bh-header"
                           }
                        >
                           {month}
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                           <List
                              className="call-list"
                              component="nav"
                              aria-label="calls"
                           >
                              {this.state.calls[month].map(call => (
                                 <ListItem key={call.id} button>
                                    <ListItemText
                                       primary={renderCallItem(
                                          month,
                                          call,
                                          this.openModal,
                                          this.deleteCall
                                       )}
                                    />
                                 </ListItem>
                              ))}
                           </List>
                        </ExpansionPanelDetails>
                     </ExpansionPanel>
                  ))}
               </Grid>
               <Grid item className="info-row">
                  <h4>Total call duration</h4>
                  <div>{this.state.totalCallDuration} sec</div>
               </Grid>
            </Grid>
         </div>
      ) : (
         <div />
      );
   }
}

const CallInfo = connect(select, dispatcher)(ConnectedCallInfo);

export default CallInfo;
