import Grid from "@material-ui/core/Grid";
import React from "react";
import { connect } from "react-redux";
import { selectCaller, selectContact } from "../../actions";
import CallInfo from "../call-info/CallInfo";
import CallerList from "../caller-list/CallerList";
import ContactList from "../contact-list/ContactList";
import "./App.css";

const fullName = caller => {
   if (caller == null) {
      return "";
   }
   return caller.first_name + " " + caller.last_name;
};

const dispatcher = dispatch => {
   return {
      selectCaller: callerId => dispatch(selectCaller(callerId)),
      selectContact: contactId => dispatch(selectContact(contactId))
   };
};

const select = store => {
   return {
      callers: store.callers
   };
};

const initialState = {
   selectedCaller: -1,
   selectedContact: -1
};

class ConnectedApp extends React.Component {
   constructor(props) {
      super(props);
      this.state = initialState;

      this.callerSelected = this.callerSelected.bind(this);
      this.contactSelected = this.contactSelected.bind(this);
      this.getCaller = this.getCaller.bind(this);
   }

   getCaller(id) {
      return this.props.callers.filter(caller => caller.id === id)[0];
   }

   callerSelected(callerId) {
      this.setState({
         selectedCaller: callerId
      });
      this.props.selectCaller(callerId);
   }

   contactSelected(contactId) {
      this.setState({
         selectedContact: contactId
      });
      this.props.selectContact(contactId);
   }

   render() {
      return (
         <Grid
            className="main-container"
            container
            direction="row"
            justify="center"
            alignItems="flex-start"
         >
            <Grid item xs={4} md={3}>
               <div className="grid-item-container">
                  <div className="caller-list-header">
                     <h3>Callers</h3>
                     <div className="caller-list-header-info">
                        Selected: &nbsp;
                        {fullName(this.getCaller(this.state.selectedCaller))}
                     </div>
                  </div>
                  <CallerList selectCaller={this.callerSelected} />
               </div>
            </Grid>
            <Grid item xs={4} md={3}>
               <div className="grid-item-container">
                  <div className="caller-list-header">
                     <h3>Placed calls</h3>
                     <div className="caller-list-header-info">
                        Selected: &nbsp;
                        {fullName(this.getCaller(this.state.selectedContact))}
                     </div>
                  </div>
                  <ContactList
                     callerId={this.state.selectedCaller}
                     selectContact={this.contactSelected}
                  />
               </div>
            </Grid>
            <Grid item xs={4} md={6}>
               <div className="grid-item-container">
                  <h3>Call information</h3>
                  <CallInfo
                     callerId={this.state.selectedCaller}
                     contactId={this.state.selectedContact}
                  />
               </div>
            </Grid>
         </Grid>
      );
   }
}

const App = connect(select, dispatcher)(ConnectedApp);

export default App;
