import { Paper, TextField } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import React from "react";
import { connect } from "react-redux";
import { fetchCallers } from "../../actions/index";
import "./CallerList.css";

const fullName = caller => {
   return caller.first_name + " " + caller.last_name;
};

const dispatcher = dispatch => {
   return {
      fetchCallers: () => dispatch(fetchCallers())
   };
};

const select = store => {
   return { callers: store.callers };
};

const initialState = {
   callers: [],
   search: ""
};

const matchCaller = (caller, query) => {
   return (
      caller.first_name.includes(query) ||
      caller.last_name.includes(query) ||
      caller.email.includes(query)
   );
};

class ConnectedCallerList extends React.Component {
   constructor(props) {
      super(props);
      this.state = initialState;

      this.search = this.search.bind(this);
   }

   search(event) {
      this.setState({
         search: event.target.value
      });
   }

   componentDidMount() {
      this.props.fetchCallers();
   }

   componentDidUpdate(prevProps) {
      if (this.state.callers.length === 0) {
         this.setState({
            callers: this.props.callers
         });
      }
   }

   render() {
      const { selectCaller } = this.props;
      const searchResults =
         this.state.search.length > 0
            ? this.state.callers.filter(caller =>
                 matchCaller(caller, this.state.search)
              )
            : this.state.callers;
      return (
         <Paper className="column">
            <div className="caller-search-wrapper">
               <TextField
                  margin="dense"
                  id="caller-search"
                  label="Search callers"
                  type="text"
                  value={this.state.search}
                  onChange={this.search}
                  fullWidth
               />
            </div>
            <div className="caller-list-wrapper">
               <List
                  className="caller-list"
                  component="nav"
                  aria-label="main callers"
               >
                  {searchResults.map(caller => (
                     <ListItem
                        key={caller.id}
                        onClick={() => selectCaller(caller.id)}
                        button
                     >
                        <ListItemText primary={fullName(caller)} />
                     </ListItem>
                  ))}
               </List>
            </div>
         </Paper>
      );
   }
}

const CallerList = connect(select, dispatcher)(ConnectedCallerList);

export default CallerList;
