### FS19 - Full Stack exercise

#### Requirements

1. Java 8
2. Maven

#### Build

To build the project run:

`mvn clean package`

#### Initialization

To initialize the application, move into the `backend` directory and
run :

`mvn spring-boot:run -Pinit`

This will initialize the local
H2 database with mock data

#### Execution

To run the application, move into the `backend` directory and run:

`mvn spring-boot:run`

Then, visit `localhost:8080` on your preferred browser
